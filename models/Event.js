const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const EventSchema = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    },
    users: [{
        type: mongoose.Types.ObjectId,
        ref: 'User'
    }],
    title: {
        type: String,
        required: [true, "Поле title обязательно для заполнения"],
    },
    datetime: {
        type: Date,
        required: [true, "Поле datetime обязательно для заполнения"],
    },
    duration: {
        type: String
    }
});


const Event = mongoose.model('Event', EventSchema);

module.exports = Event;