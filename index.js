const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require("./config");
const app = express();
const port = 8000;
const users = require("./app/users");
const events = require('./app/events');


app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const run = async () => {
    await mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

    console.log("Connected to Mongo DB");

    app.use('/events', events());
    app.use("/users", users);

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);
