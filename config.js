const path = require("path");

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public/uploads"),
  db: {
    name: "EventsCalendar",
    url: "mongodb://localhost"
  },
  fb: {
    appId: "340877070575904",
    appSecret: process.env.FB_SECRET
    // d042f6505522f3adfb0f472e41db9fb5
  }
};

