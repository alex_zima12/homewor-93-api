const router = require("express").Router();
const auth = require("../middleware/auth");
const Event = require("../models/Event");
const User = require("../models/User");

const createRouter = () => {

    router.get('/', auth, async (req, res) => {
        try {
            const events = await Event.find().sort({datetime: -1}).populate("user").populate("users");
            let eventsList = [];
            events.map(event => {
                    let now = new Date(),
                        eventDate = new Date(event.datetime)
                    if (now <= eventDate) {
                       return  eventsList.push(event)
                    }
            })
            return res.send(eventsList)
        } catch (e) {
            return res.status(400).send({error: e});
        }
    });

    router.post("/mail", auth, async (req, res) => {
        const user = await User.findOne({email: req.body.mail});
        const query = {
            user: req.user._id
        }
        if (!user) {
            return res.status(400).send({error: "Username not found"});
        }
         try {
            const event = await Event.findOne(query);
             if (!event) {
                 return res.status(400).send({error: "Event not found"});
             }
            if (event.users.includes(user))
            {
                return res.status(400).send({error: "This user has already been added"});
            } else {
                event.users = [...event.users, user]
                event.save();
                res.send({message: "Пользователь был успешно добавлен"})
            } ;
        } catch (e) {
            res.status(400).send({error: e});
        }
    });

    router.post("/", auth, async (req, res) => {
        const event = new Event({
            title: req.body.data.title,
            duration: req.body.data.duration,
            datetime: req.body.data.datetime,
            user: req.user._id,
        });
        try {
            await event.save();
            res.send(event);
        } catch (e) {
            res.status(400).send({error: e});
        }
    });

    router.delete('/:id', auth, async (req, res) => {
        const event = await Event.findById(req.params.id);
        if (event.user.toString() !== req.user._id.toString()) {
            return res.status(401).send({error: "Ошибка авторизации"});
        }
        try {
            await event.delete();
            return res.send({message: "Успешно удалено"});
        } catch (e) {
            return res.status(500).send({error: e});
        }
    });


    router.delete('/user/:id', auth, async (req, res) => {

        const user = await User.findById(req.params.id);
        const userById = {
            user: req.user._id
        }
         try {
            const event = await Event.findOne(userById);
            if (!event) {
                return res.status(400).send({error: "Event not found"});
            }
                event.user.remove(user)
                event.save();
                res.send({message: "Пользователь был успешно добавлен"})

        } catch (e) {

            res.status(400).send({error: e});
        }
    });


    return router;
};

module.exports = createRouter;